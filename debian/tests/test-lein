#!/bin/bash

export LEIN_HOME="$AUTOPKGTEST_TMP/.lein"
num_checks=1

_check_task() {
	if ! [[ "$2" == *"$1"* ]]; then
		echo "lein check $num_checks failed"
		echo "diff:"
		diff <(echo "$1") <(echo "$2")
		exit 1
	fi
	num_checks=$((num_checks + 1))
}

cd "$AUTOPKGTEST_TMP"

HELP_TEXT="Leiningen is a tool for working with Clojure projects.

Several tasks are available:
change              Rewrite project.clj with f applied to the value at key-or-path.
check               Check syntax and warn on reflection.
classpath           Write the classpath of the current project to output-file.
clean               Removes all files from paths in clean-targets for a project
compile             Compile Clojure source into .class files.
deploy              Deploy jar and pom to remote repository.
deps                Show details about dependencies.
do                  Higher-order task to perform other tasks in succession.
help                Display a list of tasks or help for a given task or subtask.
install             Install jar and pom to the local repository; typically ~/.m2.
jar                 Package up all the project's files into a jar file.
javac               Compile Java source files.
new                 Generate scaffolding for a new project based on a template.
plugin              DEPRECATED. Please use the :user profile instead.
pom                 Write a pom.xml file to disk for Maven interoperability.
release             Perform release tasks.
repl                Start a repl session either with the current project or standalone.
retest              Run only the test namespaces which failed last time around.
run                 Run the project's -main function.
search              Search Central and Clojars for published artifacts.
show-profiles       List all available profiles or display one if given an argument.
test                Run the project's tests.
trampoline          Run a task without nesting the project's JVM inside Leiningen's.
uberjar             Package up the project files and all dependencies into a jar file.
update-in           Perform arbitrary transformations on your project map.
upgrade             Upgrade Leiningen to specified version or latest stable.
vcs                 Interact with the version control system.
version             Print version for Leiningen and the current JVM.
with-profile        Apply the given task with the profile(s) specified.

Run \`lein help \$TASK\` for details.

Global Options:
  -o             Run a task offline.
  -U             Run a task after forcing update of snapshots.
  -h, --help     Print this help or help for a specific task.
  -v, --version  Print Leiningen's version.

These aliases are available:
downgrade, expands to upgrade

See also: readme, faq, tutorial, news, sample, profiles, deploying, gpg,
mixed-source, templates, and copying."
HELP_OUTPUT=`lein help 2>&1`
_check_task "$HELP_TEXT" "$HELP_OUTPUT"

NEW_APP_TEXT="Generating a project called lein-test based on the 'app' template."
NEW_APP_OUTPUT=`lein new app lein-test 2>&1`
_check_task "$NEW_APP_TEXT" "$NEW_APP_OUTPUT"

cd "$AUTOPKGTEST_TMP/lein-test"
sed -i '$i:local-repo ".m2"' project.clj

TEST_TEXT='
lein test lein-test.core-test

lein test :only lein-test.core-test/a-test

FAIL in (a-test) (core_test.clj:7)
FIXME, I fail.
expected: (= 0 1)
  actual: (not (= 0 1))

Ran 1 tests containing 1 assertions.
1 failures, 0 errors.
Tests failed.'
TEST_OUTPUT=`lein test 2>&1`
_check_task "$TEST_TEXT" "$TEST_OUTPUT"

POM_TEXT="Wrote $AUTOPKGTEST_TMP/lein-test/pom.xml"
POM_OUTPUT=`lein pom 2>&1`
_check_task "$POM_TEXT" "$POM_OUTPUT"

CHECK_TEXT="Compiling namespace lein-test.core"
CHECK_OUTPUT=`lein check 2>&1`
_check_task "$CHECK_TEXT" "$CHECK_OUTPUT"

UBERJAR_TEXT="Compiling lein-test.core
Created $AUTOPKGTEST_TMP/lein-test/target/uberjar/lein-test-0.1.0-SNAPSHOT.jar
Created $AUTOPKGTEST_TMP/lein-test/target/uberjar/lein-test-0.1.0-SNAPSHOT-standalone.jar"
UBERJAR_OUTPUT=`lein uberjar 2>&1`
_check_task "$UBERJAR_TEXT" "$UBERJAR_OUTPUT"

cd "$AUTOPKGTEST_TMP"
