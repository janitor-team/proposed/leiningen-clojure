leiningen-clojure (2.9.1-8) unstable; urgency=medium

  * Team upload.

  [ Louis-Philippe Véronneau ]
  * d/control: add nrepl-clojure and libcomplete-clojure as install
    dependency.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Wed, 13 Jul 2022 09:22:31 +0200

leiningen-clojure (2.9.1-7) unstable; urgency=medium

  * Team upload.

  [ Louis-Philippe Véronneau ]
  * d/control: tighten the nrepl versioned dependency to fix autopkgtest
    failure on cljx-clojure.
  * d/control: Standards-Version update to 4.6.1. No changes.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Tue, 12 Jul 2022 20:46:55 +0200

leiningen-clojure (2.9.1-6) unstable; urgency=medium

  * Team upload

  [ Louis-Philippe Véronneau ]
  * d/control: New email for the Clojure Team.
  * d/patches/0004: patch nrepl to use 0.9.0. (Closes: #1012824)
  * d/patches/0004: there is no "debian" version for maven-resolver-provider,
    only a "3.x" version
  * d/tests: mark test-lein as requiring network connectivity.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sun, 19 Jun 2022 12:31:29 -0400

leiningen-clojure (2.9.1-5) unstable; urgency=medium

  * Patch lein to use "debian" versions. (Closes: #981353)
  * Remove outdated dependency.
  * Fix homepage link.

 -- Elana Hashman <ehashman@debian.org>  Sat, 30 Jan 2021 23:57:40 -0500

leiningen-clojure (2.9.1-4) unstable; urgency=medium

  * Team upload.
  * d/patches: use generic "debian" version for slf4j-nop. (Closes: #980645)
  * d/tests: use $AUTOPKGTEST_TMP instead of /tmp.
  * d/control: use dh-compat and upgrade to dh13.
  * d/control: Standards-Version update to 4.5.1. Add Rules-Requires-Root.
  * d/copyright: fix insecure URI.
  * d/watch: update to v4 and use git mode.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 29 Jan 2021 13:37:42 -0500

leiningen-clojure (2.9.1-3) unstable; urgency=medium

  * Team upload.
  * Add patch to bump commons-io to 2.8.0. (Closes: #973094)

 -- Utkarsh Gupta <utkarsh@debian.org>  Mon, 23 Nov 2020 01:50:45 +0530

leiningen-clojure (2.9.1-2) unstable; urgency=medium

  * Patch lein to build with Clojure 1.10.1.

 -- Elana Hashman <ehashman@debian.org>  Thu, 26 Dec 2019 17:43:11 -0500

leiningen-clojure (2.9.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.

 -- Rob Browning <rlb@defaultvalue.org>  Tue, 23 Jul 2019 13:35:04 -0500

leiningen-clojure (2.9.0-1) unstable; urgency=medium

  * New upstream version.
  * Build Leiningen with JDK11 (Closes: #900299, #923164)
  * Build Leiningen with Leiningen, and no longer build/install lein-core.
  * Update autopkgtests to work with JDK11.
  * Added a NEWS.Debian file to inform users of the JDK8 -> 11 changes.

 -- Elana Hashman <ehashman@debian.org>  Sun, 03 Mar 2019 14:38:24 +0000

leiningen-clojure (2.8.1-9) unstable; urgency=medium

  * Fix dependency (libwagon2-java -> libwagon-java) in autopkgtests.
  * Assert compliance with policy 4.2.1. Obey the policy.

 -- Elana Hashman <ehashman@debian.org>  Sat, 20 Oct 2018 11:56:55 -0400

leiningen-clojure (2.8.1-7) unstable; urgency=medium

  [ Emmanuel Bourg ]
  * Depend on libwagon-java instead of libwagon2-java (Closes: #891175)
  * Standards-Version updated to 4.1.4
  * Switch to debhelper level 11

  [ Elana Hashman ]
  * Patch dependencies to match versions in Debian (Closes: #906374)
  * Make build as verbose as possible, per policy 4.2.0
  * Assert compliance with Standard-Version 4.2.0

 -- Elana Hashman <ehashman@debian.org>  Sat, 18 Aug 2018 18:34:24 -0400

leiningen-clojure (2.8.1-6) unstable; urgency=medium

  * Do not pin architecture in patch to bin/lein-pkg.
  * Do not pin architecture in debian/rules.
  * Remove hardcoded DEBUG=1 from debian/rules.

 -- Elana Hashman <ehashman@debian.org>  Sat, 14 Apr 2018 19:12:35 -0400

leiningen-clojure (2.8.1-5) unstable; urgency=medium

  * Fix homepage.
  * Update clojure builddep to clojure1.8. (Closes: #889533)
  * Patch commons-io version to match Debian.
  * Switch Vcs from alioth to salsa.
  * Add hard dependency on Java 8.

 -- Elana Hashman <ehashman@debian.org>  Fri, 13 Apr 2018 22:14:27 -0400

leiningen-clojure (2.8.1-4) unstable; urgency=medium

  * Improve short description for i18n/l10n. (Closes: #886106)
  * Add JDK in Recommends for javac task.

 -- Elana Hashman <ehashman@debian.org>  Sat, 06 Jan 2018 17:45:27 -0500

leiningen-clojure (2.8.1-3) unstable; urgency=medium

  * Fix copyright.

 -- Elana Hashman <ehashman@debian.org>  Thu, 28 Dec 2017 18:25:47 -0500

leiningen-clojure (2.8.1-2) unstable; urgency=medium

  * Fix package dependencies.
  * Fix broken autopkgtests.
  * Remove strip-nondeterminism to address performance issues.
  * Fix broken lein-core jar.

 -- Elana Hashman <ehashman@debian.org>  Sat, 23 Dec 2017 18:36:52 -0500

leiningen-clojure (2.8.1-1) unstable; urgency=medium

  * Initial release. (Closes: #819811)

 -- Elana Hashman <ehashman@debian.org>  Sat, 23 Dec 2017 11:36:13 -0500
